export default [
  {
    "id": 1,
    "name": "Leanne Graham",
    "email": "leanne_graham@gmail.com",
    "phone": "(555) 555-1234"
  },
  {
    "id": 2,
    "name": "Ervin Howell",
    "email": "e.howell@yahoo.com",
    "phone": "(555) 342-1831"
  },
  {
    "id": 3,
    "name": "Clementine Bauch",
    "email": "clementine@hotmail",
    "phone": "(555) 111-1234"
  },
  {
    "id": 4,
    "name": "Patricia Lebsack",
    "email": "lebsack_patrick39@gmail.com",
    "phone": "(555) 345-9876"
  },
  {
    "id": 5,
    "name": "Chelsey Dietrich",
    "email": "chelsey_chelsey@sprintpcs.com",
    "phone": "(555) 837-3434"
  },
  {
    "id": 6,
    "name": "Mrs. Dennis Schulist",
    "email": "Schulistik@gmail.com",
    "phone": "(535) 555-3444"
  },
  {
    "id": 7,
    "name": "Kurtis Weissnat",
    "email": "kurtis.weissnat@goneramp",
    "phone": "(555) 999-4567"
  },
  {
    "id": 8,
    "name": "Nicholas Runolfsdottir V",
    "email": "runolfsdottir@gmail.com",
    "phone": "(555) 222-9473"
  },
  {
    "id": 9,
    "name": "Glenna Reichert",
    "email": "glenna_re@aol.com",
    "phone": "(555) 321-231"
  },
  {
    "id": 10,
    "name": "Clementina DuBuque",
    "email": "dub_clementina@msn.com",
    "phone": "(555) 100-1234"
  }
];
