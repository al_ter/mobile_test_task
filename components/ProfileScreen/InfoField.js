import React from 'react';
import { StyleSheet, View, Text, Pressable } from 'react-native';

const InfoField = ({ fieldName, fieldValue, onPress = () => { } }) => {
  return (
    <Pressable onPress={onPress} style={{ width: '95%', marginBottom: 20 }}>
      <Text style={{ fontSize: 12, marginBottom: 5, color: '#555' }}>
        {fieldName}
      </Text>
      <View style={{ borderBottomWidth: 1, borderBottomColor: '#eee' }}>
        <Text style={{ fontSize: 16, paddingBottom: 2 }}>
          {fieldValue}
        </Text>
      </View>
    </Pressable>
  );
};

export default InfoField;
