import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, Linking } from 'react-native';
import InfoField from './InfoField';

const ProfileScreen = ({ route }) => {
  const { contact: { name, email, phone } } = route.params;

  return (
    <View style={styles.profileContainer}>
      <View style={styles.profileImage}>
        <Image
          source={{
            width: 150,
            height: 150,
            uri: `https://via.placeholder.com/150&text=${name}`,
          }}
          style={{ borderColor: '#999', borderWidth: 2 }}
        />
      </View>
      <View style={styles.profileInfo}>
        <InfoField fieldName='Name' fieldValue={name} />
        <InfoField fieldName='Email' fieldValue={email} onPress={() => Linking.openURL(`mailto:${email}`)} />
        <InfoField fieldName='Phone number' fieldValue={phone} onPress={() => Linking.openURL(`tel:${phone}`)} />
      </View>
      <View style={styles.profileButtons}>
        <TouchableOpacity style={styles.profileButton}>
          <Text>Edit</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.profileButton}>
          <Text>Delete</Text>
        </TouchableOpacity>
      </View>
      
    </View>
  );
};

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    paddingHorizontal: 50,
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  profileImage: {
    marginBottom: 20,
    height: 200,
    alignItems: 'center',
    justifyContent: 'center'
  },
  profileButtons: {
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  profileButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    minWidth: 100,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 25,
    backgroundColor: '#ebebeb'
  },
  profileInfo: {
    marginBottom: 20,
    width: '95%',
    alignItems: 'flex-start',
  },
  profileInfoField: {
    fontSize: 16
  }
});

export default ProfileScreen;
