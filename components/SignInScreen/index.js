import React, { useState, useContext } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableHighlight, Image, Keyboard, TouchableWithoutFeedback, KeyboardAvoidingView } from 'react-native';
import { AuthContext } from '../App/AppContext';
import WithLabel from '../HOC/WithLabel';

const SignInScreen = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { signIn } = useContext(AuthContext);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.innerContainer}>
          <View style={styles.logoContainer}>
            <Image
              style={{ maxWidth: '60%' }}
              source={require('../../assets/placeholder.com-logo3.png')}
              resizeMode='contain'
            />
          </View>

          <View style={{ flex: 5 }}>
            <Text style={{ fontWeight: '600', fontSize: 32, marginBottom: 10 }}>
              Sign In
              </Text>
            <Text style={{ color: '#777', fontSize: 16, marginBottom: 30 }}>
              Hi there! Nice to see you again.
              </Text>

            <WithLabel
              component={TextInput}
              label={'Email'}
              style={{ marginBottom: 20 }}
              autoFocus={false}
              placeholder='Email'
              value={email}
              onChangeText={setEmail}
            />

            <WithLabel
              component={TextInput}
              label={'Password'}
              placeholder='Password'
              secureTextEntry={true}
              style={{ marginBottom: 30 }}
              value={password}
              onChangeText={setPassword}
            />

            <TouchableHighlight
              style={styles.button}
              onPress={() => signIn({ user: email, password })}
              underlayColor='#de8c90'
            >
              <Text style={{ color: '#fff', fontSize: 16, fontWeight: '600' }}>Sign in</Text>
            </TouchableHighlight>

          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  innerContainer: {
    padding: 30,
    flex: 1,
    justifyContent: 'space-evenly'
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    alignSelf: 'center',
    backgroundColor: '#e7696e',
    height: 40,
    width: '70%',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default SignInScreen;
