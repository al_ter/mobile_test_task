import 'react-native-gesture-handler';
import React, { useReducer, useMemo } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from '../SignInScreen';
import ContactsScreen from '../ContactsScreen';
import ProfileScreen from '../ProfileScreen';
import { AuthContext } from './AppContext';
import config from '../../config';

const Stack = createStackNavigator();

const App = () => {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'SIGN_IN':
          return {
            ...prevState,
            isLoggedIn: true
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isLoggedIn: false
          };
      }
    },
    {
      isLoggedIn: false,
      token: ''
    }
  );

  const fakeAuth = ({ user, password }) => {
    return user === config.DEFAULT_USER && password === config.DEFAULT_PASS;
  };

  const authContext = useMemo(
    () => ({
      signIn: data => {
        const isVerified = fakeAuth(data);

        if (isVerified) {
          dispatch({ type: 'SIGN_IN' });
        } else {
          alert('Unknown user');
        }
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' })
    }),
    []
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {state.isLoggedIn ? (
            <>
              <Stack.Screen options={{headerShown: false}} name="Contacts" component={ContactsScreen} />
              <Stack.Screen
                name="Profile"
                options={({ route }) => ({ title: route.params.contact.name })}
                component={ProfileScreen}
              />
            </>
          ) : (
            <Stack.Screen
              options={{headerShown: false}}
              name="SignIn"
              component={SignInScreen}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;
