import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Contact = ({ contact }) => {
  const { name, email, phone } = contact;
  const navigation = useNavigation();

  return (
    <View style={styles.contact}>
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={() => navigation.navigate('Profile', { contact })}
      >
        <Text style={styles.contactName}>
          {name}
        </Text>
        <Text style={styles.contactEmail}>
          {email}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  contact: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: '#e8eaf5',
    borderRadius: 4,
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginBottom: 15,
    alignItems: 'center'
  },
  contactName: {
    fontSize: 16
  },
  contactEmail: {
    color: 'grey'
  }
});

export default Contact;
