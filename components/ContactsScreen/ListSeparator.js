import React from 'react';
import { View } from 'react-native';

const ListSeparator = () => (
  <View style={{
      height: 1,
      width: '86%',
      backgroundColor: '#CED0CE',
      marginLeft: '5%'
    }}
  />
);

export default ListSeparator;
