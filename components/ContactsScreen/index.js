import React, { useState, useEffect } from 'react';
import { StyleSheet, View, FlatList, TextInput } from 'react-native';
import Contact from './Contact';
import config from '../../config';
// import data from '../../data';

const ContactsScreen = ({ navigation }) => {
  const [contacts, setContacts] = useState([]);
  const [query, setQuery] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(config.DATA_URL);
      const data = await response.json();
      setContacts(data);
    }

    try {
      fetchData();
    } catch (error) {
      throw error;
    }
  }, []);

  const contains = (data, string) => {
    const { name, email, phone } = data;
    const formattedString = string.toLowerCase().trim();
    if (
      name.toLowerCase().includes(formattedString) ||
      email.toLowerCase().includes(formattedString) ||
      phone.includes(formattedString)
    ) {
      return true;
    }
    return false;
  };

  const sortNameAtoZ = (a, b) => {
    const charCodeA = parseInt(a.name.toLowerCase().charCodeAt(0));
    const charCodeB = parseInt(b.name.toLowerCase().charCodeAt(0));
    return charCodeA - charCodeB;
  };

  let searchResults = contacts.length > 0
    ? contacts.filter(contact => contains(contact, query)).sort(sortNameAtoZ)
    : [];

  return (
    <View style={styles.contacts}>
      <TextInput
        style={styles.searchBar}
        textStyle={{ color: '#000' }}
        placeholder='Search...'
        value={query}
        onChangeText={setQuery}
      />
      <View style={styles.contactsList}>
        <FlatList
          data={searchResults}
          renderItem={({ item }) => <Contact contact={item} />}
          keyExtractor={item => String(item.id)}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contacts: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff'
  },
  contactsList: {
    flex: 1
  },
  searchBar: {
    borderRadius: 25,
    borderColor: '#333',
    borderWidth: 1,
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginBottom: 20
  }
});

export default ContactsScreen;
