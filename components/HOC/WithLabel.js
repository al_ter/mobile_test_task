import React from 'react';
import { View, Text } from 'react-native';

const WithLabel = ({ component: Component, style, label, ...rest }) => {
  return (
    <View style={style}>
      <Text style={{ fontSize: 12, color: '#e7696e' }}>
        {label}
      </Text>

      <Component {...rest} />

      <View style={{ height: 1, backgroundColor: '#ddd' }} />
    </View>
  );
}

export default WithLabel;
